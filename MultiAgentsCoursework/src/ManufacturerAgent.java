
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import coursework_ontology.CourseworkOntology;
import coursework_ontology.elements.*;
import jade.content.AgentAction;
import jade.content.Concept;
import jade.content.ContentElement;
import jade.content.lang.Codec;
import jade.content.lang.Codec.CodecException;
import jade.content.lang.sl.SLCodec;
import jade.content.onto.Ontology;
import jade.content.onto.OntologyException;
import jade.content.onto.basic.Action;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.SequentialBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import coursework_ontology.CourseworkOntology;

public class ManufacturerAgent extends Agent 
{
	private AID tickerAgent;
	private ArrayList<AID> suppliers = new ArrayList<>();
	private ArrayList<Order> orderlist = new ArrayList<>();
	private int suppliespurchased;
	private int moneyearned;
	private ArrayList<AID> customers = new ArrayList<>();
	private boolean complete = false;
	private ArrayList<Order> completeorders = new ArrayList<>();
	
	//Things to change for eval
	private int late = 200;
	private int storage = 20;
	
	private int totalprofit;
	
	private Codec codec = new SLCodec();
	private Ontology ontology = CourseworkOntology.getInstance();
	
	@Override
	protected void setup()
	{
		getContentManager().registerLanguage(codec);
		getContentManager().registerOntology(ontology);
		
		totalprofit = 0;
		
		//add this agent to the yellow pages
		DFAgentDescription dfd = new DFAgentDescription();
		dfd.setName(getAID());
		ServiceDescription sd = new ServiceDescription();
		sd.setType("manufacturer");
		sd.setName(getLocalName() + "-manufacturer-agent");
		dfd.addServices(sd);
		try
		{
			DFService.register(this,  dfd);
		}
		catch(FIPAException e)
		{
			e.printStackTrace();
		}
		
		addBehaviour(new TickerWaiter(this));
	}
	
	protected void takeDown()
	{
		//Deregister from the yellow pages
		try
		{
			DFService.deregister(this);
		}
		catch(FIPAException e)
		{
			e.printStackTrace();
		}
	}
	
	public class TickerWaiter extends CyclicBehaviour
	{
		//behaviour to wait for a new day
		public TickerWaiter(Agent a)
		{
			super(a);
		}
		
		@Override
		public void action()
		{
			MessageTemplate mt = MessageTemplate.or(MessageTemplate.MatchContent("new day"), MessageTemplate.MatchContent("terminate"));
			ACLMessage msg = myAgent.receive(mt);
			if(msg != null)
			{
				if(tickerAgent == null)
				{
					tickerAgent = msg.getSender();
				}
				if(msg.getContent().equals("new day"))
				{

					suppliespurchased = 0;
					//Each order needs ticked down, so that we can tell which ones are late. 
					//The suppliers will do something similar and send the ones that are due.
					for(Order order : orderlist)
					{
						order.setDueindays(order.getDueindays()-1);
					}
					
					myAgent.addBehaviour(new FindCustomer(myAgent));
					myAgent.addBehaviour(new FindSuppliers(myAgent));
					CyclicBehaviour ro = new ReceiveOrder(myAgent);
					CyclicBehaviour rp = new ReceiveParts(myAgent);
					myAgent.addBehaviour(ro);
					myAgent.addBehaviour(rp);
					ArrayList<Behaviour> cyclicBehaviours = new ArrayList<>();
					cyclicBehaviours.add(ro);
					cyclicBehaviours.add(rp);
					myAgent.addBehaviour(new EndDayListener(myAgent, cyclicBehaviours));

				}
			}
		}
	}
	
	public class FindCustomer extends OneShotBehaviour
	{
		public FindCustomer(Agent a)
		{
			super(a);
		}
		
		@Override
		public void action()
		{
			DFAgentDescription customerTemplate = new DFAgentDescription();
			ServiceDescription sd = new ServiceDescription();
			sd.setType("customer");
			customerTemplate.addServices(sd);
			try
			{
				customers.clear();
				DFAgentDescription[] agentsType1 = DFService.search(myAgent, customerTemplate);
				for(int i=0; i < agentsType1.length; i++)
				{
					customers.add(agentsType1[i].getName());
				}
			}
			catch(FIPAException e)
			{
				e.printStackTrace();
			}
		}
	}
	
	public class FindSuppliers extends OneShotBehaviour 
	{
		public FindSuppliers(Agent a)
		{
			super(a);
		}
		
		@Override
		public void action()
		{
			suppliers.clear();
			DFAgentDescription supplieroneTemplate = new DFAgentDescription();
			ServiceDescription sd1 = new ServiceDescription();
			sd1.setType("supplierone");
			supplieroneTemplate.addServices(sd1);
			try
			{

				DFAgentDescription[] agentsType1 = DFService.search(myAgent, supplieroneTemplate);
				for (int i = 0; i < agentsType1.length; i++)
				{
					suppliers.add(agentsType1[i].getName());
				}
			}
			catch(FIPAException e)
			{
				e.printStackTrace();
			}
			
			DFAgentDescription suppliertwoTemplate = new DFAgentDescription();
			ServiceDescription sd2 = new ServiceDescription();
			sd2.setType("suppliertwo");
			suppliertwoTemplate.addServices(sd2);
			try
			{

				DFAgentDescription[] agentsType1 = DFService.search(myAgent, suppliertwoTemplate);
				for (int i = 0; i < agentsType1.length; i++)
				{
					suppliers.add(agentsType1[i].getName());
				}
			}
			catch(FIPAException e)
			{
				e.printStackTrace();
			}
			
			DFAgentDescription supplierthreeTemplate = new DFAgentDescription();
			ServiceDescription sd3 = new ServiceDescription();
			sd3.setType("supplierthree");
			supplieroneTemplate.addServices(sd3);
			try
			{

				DFAgentDescription[] agentsType1 = DFService.search(myAgent, supplierthreeTemplate);
				for (int i = 0; i < agentsType1.length; i++)
				{
					suppliers.add(agentsType1[i].getName());
				}
			}
			catch(FIPAException e)
			{
				e.printStackTrace();
			}
		}
	}

	public class ReceiveOrder extends CyclicBehaviour
	{
		public ReceiveOrder(Agent a)
		{
			super(a);
		}
		
		@Override
		public void action()
		{
			//This behaviour should only respond to REQUEST messages
			MessageTemplate mt = MessageTemplate.MatchPerformative(ACLMessage.REQUEST);
			ACLMessage msg = receive(mt);
			if(msg != null)
			{
				try
				{
					System.out.println("MANUFACTURER: Order received");
					ContentElement ce = null;
					
					ce = getContentManager().extractContent(msg);
					if(ce instanceof Action)
					{
						
						Concept action = ((Action)ce).getAction();
						if (action instanceof Send)
						{
							//Order is now added
							orderlist.add(((Send) action).getOrder());
							System.out.println("MANUFACTURER: Order saved to list");
							myAgent.addBehaviour(new OrderParts(myAgent));
							
						}
					}
				}
				catch(Exception e)
				{
					e.printStackTrace();
					System.out.println("MANUFACTURER: Failed to save order");
				}
				
				//Got to inform the customer that we received the order, so they don't finish the day prematurely
				ACLMessage reply = msg.createReply();
				reply.setPerformative(ACLMessage.INFORM);
				reply.setContent("received");
				myAgent.send(reply);
				System.out.println("MANUFACTURER: Acknowledgement of order received sent.");

			}

		}
	}
	
	public class ReceiveParts extends CyclicBehaviour
	{
		public ReceiveParts(Agent a)
		{
			super(a);
		}
		
		@Override
		public void action()
		{
			//This behaviour should only respond to CONFIRM messages
			MessageTemplate mt = MessageTemplate.MatchPerformative(ACLMessage.CONFIRM);
			ACLMessage msg = receive(mt);
			if(msg != null)
			{
				try
				{
					System.out.println("MANUFACTURER: Parts received");
					ContentElement ce = null;
					
					ce = getContentManager().extractContent(msg);
					if(ce instanceof Action)
					{
						
						Concept action = ((Action)ce).getAction();
						if (action instanceof Send)
						{
							//Order is now added
							completeorders.add(((Send) action).getOrder());
							System.out.println("MANUFACTURER: Parts ready to send");							
						}
					}
				}
				catch(Exception e)
				{
					e.printStackTrace();
					System.out.println("MANUFACTURER: Failed to save order");
				}
				
				myAgent.addBehaviour(new FulfilOrders(myAgent));
				
			}

		}
	}
	
	public class OrderParts extends OneShotBehaviour
	{
		public OrderParts(Agent a)
		{
			super(a);
		}
		
		@Override
		public void action()
		{
			AID supplierAgent = null;
			
			//This is where the most recent order is requested from the suppliers
			System.out.println("MANUFACTURER: Ordering Parts...");
			if (orderlist.get(orderlist.size()-1).getDueindays() > 7)
			{
				//Order from supplier 3
				DFAgentDescription supplierthreeTemplate = new DFAgentDescription();
				ServiceDescription sd = new ServiceDescription();
				sd.setType("supplierthree");
				supplierthreeTemplate.addServices(sd);
				try
				{
					DFAgentDescription[] agentsType1 = DFService.search(myAgent,  supplierthreeTemplate);
					supplierAgent = agentsType1[0].getName();
					
				}
				catch(FIPAException e)
				{
					e.printStackTrace();
				}
				
				ACLMessage msg = new ACLMessage(ACLMessage.REQUEST);
				msg.addReceiver(supplierAgent);
				msg.setLanguage(codec.getName());
				msg.setOntology(ontology.getName());
				try
				{
					AgentAction s = new Send(myAgent.getAID(), orderlist.get(orderlist.size()-1));
					Action actionOp = new Action(myAgent.getAID(), s);
					getContentManager().fillContent(msg, actionOp);
					
					myAgent.send(msg);
					System.out.println("MANUFACTURER: Order sent to supplier three");
				}
				catch (CodecException e) 
				{
					e.printStackTrace();
				} 
				catch (OntologyException e) 
				{
					e.printStackTrace();
				}
				//Receive price
				MessageTemplate mt = MessageTemplate.MatchPerformative(ACLMessage.PROPOSE);
				ACLMessage pricemessage = myAgent.receive(mt);
				
				while(pricemessage == null)
				{
					block();
					pricemessage = myAgent.receive(mt);
				}
				
				if(pricemessage != null)
				{
					System.out.println("MANUFACTURER: Price received.");
					suppliespurchased = Integer.parseInt(pricemessage.getContent());
					complete = true;
				}

			}
			else if ((orderlist.get(orderlist.size()-1).getDueindays() >= 3) || (orderlist.get(orderlist.size()-1).getDueindays() <= 7))
			{
				//Order from supplier 2
				DFAgentDescription suppliertwoTemplate = new DFAgentDescription();
				ServiceDescription sd = new ServiceDescription();
				sd.setType("suppliertwo");
				suppliertwoTemplate.addServices(sd);
				try
				{
					DFAgentDescription[] agentsType1 = DFService.search(myAgent,  suppliertwoTemplate);
					supplierAgent = agentsType1[0].getName();
				}
				catch(FIPAException e)
				{
					e.printStackTrace();
				}
				
				ACLMessage msg = new ACLMessage(ACLMessage.REQUEST);
				msg.addReceiver(supplierAgent);
				msg.setLanguage(codec.getName());
				msg.setOntology(ontology.getName());
				try
				{
					AgentAction s = new Send(myAgent.getAID(), orderlist.get(orderlist.size()-1));
					Action actionOp = new Action(myAgent.getAID(), s);
					getContentManager().fillContent(msg, actionOp);
					
					myAgent.send(msg);
					System.out.println("MANUFACTURER: Order sent to supplier two");
				}
				catch (CodecException e) 
				{
					e.printStackTrace();
				}
				catch (OntologyException e) 
				{
					e.printStackTrace();
				}
				//Receive price
				MessageTemplate mt = MessageTemplate.MatchPerformative(ACLMessage.PROPOSE);
				ACLMessage pricemessage = myAgent.receive(mt);
				
				while(pricemessage == null)
				{
					block();
					pricemessage = myAgent.receive(mt);
				}
				
				if(pricemessage != null)
				{
					System.out.println("MANUFACTURER: price " + pricemessage);
					System.out.println("MANUFACTURER: Price received.");
					suppliespurchased = Integer.parseInt(pricemessage.getContent());
					complete = true;
				}

			}
			else if (orderlist.get(orderlist.size()-1).getDueindays() < 3)
			{
				//Order from supplier 1
				DFAgentDescription supplieroneTemplate = new DFAgentDescription();
				ServiceDescription sd = new ServiceDescription();
				sd.setType("supplierone");
				supplieroneTemplate.addServices(sd);
				try
				{
					DFAgentDescription[] agentsType1 = DFService.search(myAgent,  supplieroneTemplate);
					supplierAgent = agentsType1[0].getName();
				}
				catch(FIPAException e)
				{
					e.printStackTrace();
				}
				
				ACLMessage msg = new ACLMessage(ACLMessage.REQUEST);
				msg.addReceiver(supplierAgent);
				msg.setLanguage(codec.getName());
				msg.setOntology(ontology.getName());
				try
				{
					AgentAction s = new Send(myAgent.getAID(), orderlist.get(orderlist.size()-1));
					Action actionOp = new Action(myAgent.getAID(), s);
					getContentManager().fillContent(msg, actionOp);
					
					myAgent.send(msg);
					System.out.println("MANUFACTURER: Order sent to supplier one");
				}
				catch (CodecException e)
				{
					e.printStackTrace();
				} 
				catch (OntologyException e) 
				{
					e.printStackTrace();
				}
				//Receive price
				MessageTemplate mt = MessageTemplate.MatchPerformative(ACLMessage.PROPOSE);
				ACLMessage pricemessage = myAgent.receive(mt);
				
				while(pricemessage == null)
				{
					block();
					pricemessage = myAgent.receive(mt);
				}
				
				if(pricemessage != null)
				{
					System.out.println("MANUFACTURER: Price received.");
					suppliespurchased = Integer.parseInt(pricemessage.getContent());
					complete = true;
				}
			}
			
			
		}
	}
	
	public class FulfilOrders extends OneShotBehaviour
	{
		public FulfilOrders(Agent a)
		{
			super(a);
		}
		
		@Override
		public void action()
		{
			System.out.println("MANUFACTURER: Attempting to fulfil orders");
			//Iterator<Order> ord = orderlist.iterator();
			//Iterator<Order> par = completeorders.iterator();
			for (Iterator<Order> par = completeorders.iterator(); par.hasNext();)
			{
				Order parts = par.next();
				//Need to check it's the same computer and price, but the objects are different
				
				System.out.println("MANUFACTURER: Orders match, sending");
				//Send it out
				ACLMessage fulfilment = new ACLMessage(ACLMessage.CONFIRM);
				for(AID customer : customers)
				{
					fulfilment.addReceiver(customer);						
				}
				fulfilment.setLanguage(codec.getName());
				fulfilment.setOntology(ontology.getName());
				try
				{
					AgentAction s = new Send(myAgent.getAID(), parts);
					Action actionOp = new Action(myAgent.getAID(), s);
					getContentManager().fillContent(fulfilment, actionOp);
					System.out.println("MANUFACTURER: Order complete, sending");
					myAgent.send(fulfilment);
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
				
				//Receive fat stacks
				MessageTemplate mt = MessageTemplate.MatchPerformative(ACLMessage.AGREE);
				ACLMessage pricemessage = myAgent.receive(mt);
				
				par.remove();
				
				while(pricemessage == null)
				{
					block();
					pricemessage = myAgent.receive(mt);
				}
				
				if(pricemessage != null)
				{
					System.out.println("MANUFACTURER: Payment received.");
					moneyearned = Integer.parseInt(pricemessage.getContent());
					complete = true;
				}
				
			}
		}
	}
	
	public class CalculateProfits extends OneShotBehaviour
	{
		public CalculateProfits(Agent a)
		{
			super(a);
		}
		
		@Override
		public void action()
		{
			int warehouse = 0;
			int latepen = 0;
			
			//Profits are calculated as:
			//shippedorders - latepenalty - warehousestorage - suppliespurchased
			
			//This loop will be used to calculate warehouse storage and late penalties
			for (Order order : completeorders)
			{
				//Calculate lateness penalty
				if (order.getDueindays() <= 0)
				{
					latepen = latepen + late;
				}
				

			}
			
			for (Order order : completeorders)
			{
				if (order.getPc().getDesktop() == null)
				{
					//is a laptop
					warehouse = (storage * 6) * order.getQuantity();
				}
			}
			
			int profit = moneyearned - latepen - warehouse - suppliespurchased;
			totalprofit = totalprofit + profit;
			System.out.println("MANUFACTURER: Earnings: " + moneyearned);
			System.out.println("MANUFACTURER: Late pen: " + latepen);
			System.out.println("MANUFACTURER: Storage : " + warehouse);
			System.out.println("MANUFACTURER: Supplies: " + suppliespurchased);
			System.out.println("MANUFACTURER: Profit  : " + profit);
			System.out.println("MANUFACTURER: TTD     : " + totalprofit);
		}
	}
	
	public class EndDayListener extends CyclicBehaviour
	{
		private int customerFinished = 0;
		private List<Behaviour> toRemove;
		
		public EndDayListener(Agent a, List<Behaviour> toRemove)
		{
			super(a);
			this.toRemove = toRemove;
		}
		
		@Override
		public void action()
		{
			if(!complete)
			{
				block();
			}
			else
			{
				MessageTemplate mt = MessageTemplate.MatchContent("done");
				ACLMessage msg = myAgent.receive(mt);
				
				if(msg != null)
				{
					customerFinished++;
				}
				else
				{
					block();
				}
				
				if (customerFinished == customers.size())
				{
					//Manufacturer is done for the day
					ACLMessage tick = new ACLMessage(ACLMessage.INFORM);
					tick.setContent("done");
					tick.addReceiver(tickerAgent);
					for(AID supplier : suppliers)
					{
						tick.addReceiver(supplier);
					}
					myAgent.send(tick);
					System.out.println("MANUFACTURER: Day end, ticker agent and suppliers informed");
					myAgent.addBehaviour(new CalculateProfits(myAgent));
					
					//Remove the cyclic behaviours
					for (Behaviour b : toRemove)
					{
						myAgent.removeBehaviour(b);
					}
					complete = false;
					myAgent.removeBehaviour(this);
				}
			}
			
		}
	}
}

import java.util.ArrayList;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

public class TickerAgent extends Agent
{
	//Number of days in the simulation
	public static final int NUM_DAYS = 90;
	
	@Override
	protected void setup()
	{
		//add this agent to the yellow pages
		DFAgentDescription dfd = new DFAgentDescription();
		dfd.setName(getAID());
		ServiceDescription sd = new ServiceDescription();
		sd.setType("ticker-agent");
		sd.setName(getLocalName() + "-ticker-agent");
		dfd.addServices(sd);
		try
		{
			DFService.register(this, dfd);
		}
		catch(FIPAException e)
		{
			e.printStackTrace();
		}
		
		//wait for the other agents to start
		addBehaviour(new SynchAgentsBehaviour(this));
	}
	
	@Override
	protected void takeDown()
	{
		//Deregister from the yellow pages
		try
		{
			DFService.deregister(this);
		}
		catch(FIPAException e)
		{
			e.printStackTrace();
		}
	}
	
	public class SynchAgentsBehaviour extends Behaviour
	{
		private int step = 0; //where we are in the behaviour
		private int numFinReceived = 0; //number of finished messages from other agents
		private int day = 0;
		private ArrayList<AID> simulationAgents = new ArrayList<>();
		
		public SynchAgentsBehaviour(Agent a)
		{
			super(a);
		}
		
		@Override
		public void action()
		{
			switch(step)
			{
			case 0:
				//find all agents using directory service

				DFAgentDescription customer = new DFAgentDescription();
				ServiceDescription customersd = new ServiceDescription();
				customersd.setType("customer");
				customer.addServices(customersd);
				
				DFAgentDescription manufacturer = new DFAgentDescription();
				ServiceDescription manufacturersd = new ServiceDescription();
				manufacturersd.setType("manufacturer");
				manufacturer.addServices(manufacturersd);
				
				DFAgentDescription supplierone = new DFAgentDescription();
				ServiceDescription supplieronesd = new ServiceDescription();
				supplieronesd.setType("supplierone");
				supplierone.addServices(supplieronesd);
				
				DFAgentDescription suppliertwo = new DFAgentDescription();
				ServiceDescription suppliertwosd = new ServiceDescription();
				suppliertwosd.setType("suppliertwo");
				suppliertwo.addServices(suppliertwosd);
				
				DFAgentDescription supplierthree = new DFAgentDescription();
				ServiceDescription supplierthreesd = new ServiceDescription();
				supplierthreesd.setType("supplierthree");
				supplierthree.addServices(supplierthreesd);
				doWait(15000);
				try
				{
					simulationAgents.clear();
					//search for agents of type "simulation-agent"
					DFAgentDescription[] agentsType1 = DFService.search(myAgent, customer);
					for(int i=0; i<agentsType1.length; i++)
					{
						simulationAgents.add(agentsType1[i].getName()); //this is the AID
						System.out.println(agentsType1[i].getName());
					}
					//search for agents of type "simulation-agent2"
					
					DFAgentDescription[] agentsType2 = DFService.search(myAgent,  manufacturer);
					for(int i=0; i<agentsType2.length; i++)
					{
						simulationAgents.add(agentsType2[i].getName()); //this is the AID
						System.out.println(agentsType2[i].getName());
					}
					
					DFAgentDescription[] agentsType3 = DFService.search(myAgent,  supplierone);
					for(int i=0; i<agentsType3.length; i++)
					{
						simulationAgents.add(agentsType3[i].getName()); //this is the AID
						System.out.println(agentsType3[i].getName());
					}
					
					DFAgentDescription[] agentsType4 = DFService.search(myAgent,  suppliertwo);
					for(int i=0; i<agentsType4.length; i++)
					{
						simulationAgents.add(agentsType4[i].getName()); //this is the AID
						System.out.println(agentsType4[i].getName());
					}
					
					DFAgentDescription[] agentsType5 = DFService.search(myAgent,  supplierthree);
					for(int i=0; i<agentsType5.length; i++)
					{
						simulationAgents.add(agentsType5[i].getName()); //this is the AID
						System.out.println(agentsType5[i].getName());
					}
				}
				catch(FIPAException e)
				{
					e.printStackTrace();
				}
				
				//send new day message to each agent
				ACLMessage tick = new ACLMessage(ACLMessage.INFORM);
				tick.setContent("new day"); //the message content
				System.out.println("TICKER: NEW DAY");
				for(AID id : simulationAgents)
				{
					tick.addReceiver(id);
				}
				myAgent.send(tick);
				step++;
				day++;
				break;
			case 1:
				
				//wait to receive a "done" message from all agents
				MessageTemplate mt = MessageTemplate.MatchContent("done");
				ACLMessage msg = myAgent.receive(mt);
				if(msg != null)
				{
					numFinReceived++;
					if(numFinReceived >= simulationAgents.size()) 
					{
						step++;
					}
				}
				else
				{
					block();
				}
				
			}
		}
		
		@Override
		public boolean done()
		{
			return step == 2;
		}
		
		@Override
		public void reset()
		{
			step = 0;
			simulationAgents.clear();
			numFinReceived = 0;
		}
		
		@Override
		public int onEnd()
		{
			System.out.println("TICKER: End of day: " + day);
			if(day == NUM_DAYS)
			{
				//send termination message to each agent
				ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
				msg.setContent("terminate");
				for(AID agent : simulationAgents)
				{
					msg.addReceiver(agent);
				}
				myAgent.send(msg);
				myAgent.doDelete();
			}
			else
			{
				reset();
				myAgent.addBehaviour(this);				
			}

			return 0;
		}
	}
}

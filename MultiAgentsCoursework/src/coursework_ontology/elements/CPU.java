package coursework_ontology.elements;

import jade.content.Concept;
import jade.content.onto.annotations.Slot;

public class CPU implements Concept
{
	private String speed;
	
	@Slot (mandatory = true)
	public String getSpeed()
	{
		return speed;
	}
	
	public void setSpeed(String speed)
	{
		this.speed = speed;
	}
}

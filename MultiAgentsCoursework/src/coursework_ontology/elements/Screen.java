package coursework_ontology.elements;

import jade.content.Concept;
import jade.content.onto.annotations.Slot;

public class Screen implements Concept 
{
	private boolean laptopscreen;
	
	@Slot (mandatory = true)
	public boolean getLaptopScreen()
	{
		return laptopscreen;
	}
	
	public void setLaptopScreen(boolean laptopscreen)
	{
		this.laptopscreen = laptopscreen;
	}
}

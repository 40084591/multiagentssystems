package coursework_ontology.elements;

import jade.content.Concept;
import jade.content.onto.annotations.Slot;

public class PC implements Concept 
{
	
	private OS os;
	private Ram ram;
	private Harddrive harddrive;
	
	//Either laptop or desktop
	private Laptop laptop;
	private Desktop desktop;
	
	public PC()
	{
		super();
	}
	
	//Create a laptop
	public PC (OS os, Ram ram, Harddrive harddrive, Laptop laptop)
	{
		this.os = os;
		this.ram = ram;
		this.harddrive = harddrive;
		this.laptop = laptop;
	}
	
	//Create a desktop
	public PC (OS os, Ram ram, Harddrive harddrive, Desktop desktop)
	{
		this.os = os;
		this.ram = ram;
		this.harddrive = harddrive;
		this.desktop = desktop;
	}
	
	@Slot (mandatory = true)
	public OS getOs() {
		return os;
	}
	public void setOs(OS os) {
		this.os = os;
	}
	
	@Slot (mandatory = true)
	public Ram getRam() {
		return ram;
	}
	public void setRam(Ram ram) {
		this.ram = ram;
	}
	
	@Slot (mandatory = true)
	public Harddrive getHarddrive() {
		return harddrive;
	}
	public void setHarddrive(Harddrive harddrive) {
		this.harddrive = harddrive;
	}
	
	public Laptop getLaptop() 
	{
		return laptop;
	}
	
	public void setLaptop(Laptop laptop) 
	{
		this.laptop = laptop;
	}
	
	public Desktop getDesktop() 
	{
		return desktop;
	}
	public void setDesktop(Desktop desktop) 
	{
		this.desktop = desktop;
	}

}

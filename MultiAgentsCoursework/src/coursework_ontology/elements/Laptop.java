package coursework_ontology.elements;

import jade.content.Concept;
import jade.content.onto.annotations.Slot;

public class Laptop implements Concept {

	private CPU cpu;
	private Motherboard motherboard;
	private Screen laptopscreen;
	
	@Slot (mandatory = true)
	public CPU getCpu() {
		return cpu;
	}
	public void setCpu(CPU cpu) {
		this.cpu = cpu;
	}
	
	@Slot (mandatory = true)
	public Motherboard getMotherboard() {
		return motherboard;
	}
	public void setMotherboard(Motherboard motherboard) {
		this.motherboard = motherboard;
	}
	
	@Slot (mandatory = true)
	public Screen isLaptopscreen() {
		return laptopscreen;
	}
	public void setLaptopscreen(Screen laptopscreen) {
		this.laptopscreen = laptopscreen;
	}
	
}

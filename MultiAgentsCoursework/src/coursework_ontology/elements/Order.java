package coursework_ontology.elements;

import jade.content.AgentAction;
import jade.core.AID;

public class Order implements AgentAction
{
	private AID customer;
	private PC pc;
	private int dueindays;
	private int quantity;
	private int price;

	private int daystofulfil;
	
	public AID getCustomer() 
	{
		return customer;
	}
	
	public void setCustomer(AID customer) 
	{
		this.customer = customer;
	}
	
	
	public PC getPc() 
	{
		return pc;
	}
	public void setPc(PC pc) 
	{
		this.pc = pc;
	}
	
	
	
	public int getDueindays() 
	{
		return dueindays;
	}
	public void setDueindays(int dueindays) 
	{
		this.dueindays = dueindays;
	}
	
	
	public int getQuantity()
	{
		return quantity;
	}
	public void setQuantity(int quantity)
	{
		this.quantity = quantity;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}
	
	public int getDaystofulfil() {
		return daystofulfil;
	}

	public void setDaystofulfil(int daystofulfil) {
		this.daystofulfil = daystofulfil;
	}

	
	
	
}

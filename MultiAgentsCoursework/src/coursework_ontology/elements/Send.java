package coursework_ontology.elements;

import jade.content.AgentAction;
import jade.core.AID;

public class Send implements AgentAction
{
	private AID sender;
	private Order order;
	
	public Send()
	{
		super();
	}
	
	public Send(AID sender, Order order) {
		super();
		this.sender = sender;
		this.order = order;
	}
	public AID getSender() {
		return sender;
	}
	public void setSender(AID sender) {
		this.sender = sender;
	}
	public Order getOrder() {
		return order;
	}
	public void setOrder(Order order) {
		this.order = order;
	}
	
	
}

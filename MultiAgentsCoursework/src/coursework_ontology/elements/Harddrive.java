package coursework_ontology.elements;

import jade.content.Concept;
import jade.content.onto.annotations.Slot;

public class Harddrive implements Concept
{
	private int capacity;
	
	@Slot (mandatory = true)
	public int getCapacity()
	{
		return capacity;
	}
	
	public void setCapacity(int capacity)
	{
		this.capacity = capacity;
	}
}

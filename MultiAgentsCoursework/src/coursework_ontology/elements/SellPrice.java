package coursework_ontology.elements;

import jade.content.Concept;
import jade.content.Predicate;
import jade.core.AID;

public class SellPrice implements Predicate
{
	private AID supplier;
	private Concept item;
	private int price;
	
	public AID getSupplier() {
		return supplier;
	}
	public void setSupplier(AID supplier) {
		this.supplier = supplier;
	}
	public Concept getItem() {
		return item;
	}
	public void setItem(Concept item) {
		this.item = item;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
}

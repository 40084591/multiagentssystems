package coursework_ontology.elements;

import jade.content.Concept;
import jade.content.onto.annotations.Slot;

public class OS implements Concept {

	private String version;
	
	@Slot (mandatory = true)
	public String getVersion() 
	{
		return version;
	}
	
	public void setVersion(String version)
	{
		this.version = version;
	}
}

package coursework_ontology;

import jade.content.onto.BeanOntology;
import jade.content.onto.BeanOntologyException;
import jade.content.onto.Ontology;

public class CourseworkOntology extends BeanOntology
{

	private static Ontology singleInstance = new CourseworkOntology("ontology");
	
	public static Ontology getInstance()
	{
		return singleInstance;
	}
	
	private CourseworkOntology(String name)
	{
		super(name);
		try
		{
			add("coursework_ontology.elements");
		}
		catch(BeanOntologyException e)
		{
			e.printStackTrace();
		}
	}
	
}

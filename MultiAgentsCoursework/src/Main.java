
import jade.core.*;
import jade.core.Runtime;
import jade.wrapper.AgentController;
import jade.wrapper.ContainerController;


public class Main {

	public static void main(String[] args) {
		Profile myProfile = new ProfileImpl();
		Runtime myRuntime = Runtime.instance();
		try{
			ContainerController myContainer = myRuntime.createMainContainer(myProfile);	
			AgentController rma = myContainer.createNewAgent("rma", "jade.tools.rma.rma", null);
			rma.start();
			
			AgentController tickerAgent = myContainer.createNewAgent("ticker", TickerAgent.class.getCanonicalName(), null);
			tickerAgent.start();
			
			AgentController customerAgent = myContainer.createNewAgent("customer", CustomerAgent.class.getCanonicalName(), null);
			customerAgent.start();
			
			AgentController customerAgent2 = myContainer.createNewAgent("customer2", CustomerAgent.class.getCanonicalName(), null);
			customerAgent2.start();
			
			AgentController customerAgent3 = myContainer.createNewAgent("customer3", CustomerAgent.class.getCanonicalName(), null);
			customerAgent3.start();
			
			AgentController manufacturerAgent = myContainer.createNewAgent("manufacturer", ManufacturerAgent.class.getCanonicalName(), null);
			manufacturerAgent.start();
			
			AgentController supplierOne = myContainer.createNewAgent("supplier one", SupplierOneAgent.class.getCanonicalName(), null);
			supplierOne.start();
			
			AgentController supplierTwo = myContainer.createNewAgent("supplier two", SupplierTwoAgent.class.getCanonicalName(), null);
			supplierTwo.start();
			
			AgentController supplierThree = myContainer.createNewAgent("supplier three", SupplierThreeAgent.class.getCanonicalName(), null);
			supplierThree.start();

			
		}
		catch(Exception e){
			System.out.println("Exception starting agent: " + e.toString());
		}


	}

}

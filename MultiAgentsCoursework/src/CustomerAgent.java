
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import coursework_ontology.elements.*;
import jade.content.AgentAction;
import jade.content.Concept;
import jade.content.ContentElement;
import jade.content.lang.Codec;
import jade.content.lang.Codec.CodecException;
import jade.content.lang.sl.SLCodec;
import jade.content.onto.Ontology;
import jade.content.onto.OntologyException;
import jade.content.onto.basic.Action;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.SequentialBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import coursework_ontology.CourseworkOntology;

public class CustomerAgent extends Agent
{
	private AID tickerAgent;
	private AID manufacturerAgent;
	private ArrayList<AID> manufacturers = new ArrayList<>();
	private Order order = new Order();
	private int ordersSent = 0;
	private int orderReceived = 0;
	
	private Codec codec = new SLCodec();
	private Ontology ontology = CourseworkOntology.getInstance();
	
	@Override
	protected void setup()
	{
		getContentManager().registerLanguage(codec);
		getContentManager().registerOntology(ontology);
		
		//add this agent to the yellow pages
		DFAgentDescription dfd = new DFAgentDescription();
		dfd.setName(getAID());
		ServiceDescription sd = new ServiceDescription();
		sd.setType("customer");
		sd.setName(getLocalName() + "-customer-agent");
		dfd.addServices(sd);
		try
		{
			DFService.register(this, dfd);
		}
		catch(FIPAException e)
		{
			e.printStackTrace();
		}
		
		addBehaviour(new TickerWaiter(this));
	}
	
	protected void takeDown()
	{
		//Deregister from the yellow pages
		try
		{
			DFService.deregister(this);
		}
		catch(FIPAException e)
		{
			e.printStackTrace();
		}
	}
	
	public class TickerWaiter extends CyclicBehaviour
	{
		//behaviour to wait for a new day
		public TickerWaiter(Agent a)
		{
			super(a);
		}
		
		@Override
		public void action()
		{
			MessageTemplate mt = MessageTemplate.or(MessageTemplate.MatchContent("new day"), MessageTemplate.MatchContent("terminate"));
			ACLMessage msg = myAgent.receive(mt);
			if(msg != null)
			{
				if(tickerAgent == null)
				{
					tickerAgent = msg.getSender();
				}
				if(msg.getContent().equals("new day"))
				{
					//Sequential behaviour to control day's activities
					SequentialBehaviour dailyActivity = new SequentialBehaviour();
					
					//start new behaviours for the day's activities
					dailyActivity.addSubBehaviour(new FindManufacturer(myAgent));
					dailyActivity.addSubBehaviour(new GenerateOrder(myAgent));
					CyclicBehaviour ro = new ReceiveOrder(myAgent);
					myAgent.addBehaviour(ro);
					ArrayList<Behaviour> cyclicBehaviours = new ArrayList<>();
					//cyclicBehaviours.add(ro);
					dailyActivity.addSubBehaviour(new EndDay(myAgent, cyclicBehaviours));
					
					myAgent.addBehaviour(dailyActivity);
				}
			}
		}
	}
	
	public class FindManufacturer extends OneShotBehaviour 
	{
		public FindManufacturer(Agent a)
		{
			super(a);
		}
		
		@Override
		public void action()
		{
			DFAgentDescription manufacturerTemplate = new DFAgentDescription();
			ServiceDescription sd = new ServiceDescription();
			sd.setType("manufacturer");
			manufacturerTemplate.addServices(sd);
			try
			{
				manufacturers.clear();
				DFAgentDescription[] agentsType1 = DFService.search(myAgent, manufacturerTemplate);
				for (int i = 0; i < agentsType1.length; i++)
				{
					manufacturers.add(agentsType1[i].getName());
				}
			}
			catch(FIPAException e)
			{
				e.printStackTrace();
			}
		}
	}
	
	public class GenerateOrder extends OneShotBehaviour
	{
		public GenerateOrder(Agent a)
		{
			super(a);
		}
		
		@Override
		public void action()
		{	
			//Generate an order using RNG
			OS os = new OS();
			Ram ram = new Ram();
			Harddrive harddrive = new Harddrive();
			Random rand = new Random();
			
			//Generate OS
			if(rand.nextFloat() < 0.5)
			{
				os.setVersion("windows");
			}
			else 
			{
				os.setVersion("linux");
			}
			
			//Generate RAM
			if(rand.nextFloat() < 0.5)
			{
				ram.setCapacity(8);
			}
			else 
			{
				ram.setCapacity(16);
			}
			
			//Generate hard drive storage
			if(rand.nextFloat() < 0.5)
			{
				harddrive.setCapacity(1);
			}
			else
			{
				harddrive.setCapacity(2);
			}
			
			//Generate Desktop or Laptop
			if(rand.nextFloat() < 0.5)
			{
				Desktop desktop = new Desktop();
				CPU cpu = new CPU();
				Motherboard motherboard = new Motherboard();
				
				cpu.setSpeed("desktopCPU");
				motherboard.setType("desktopMotherboard");
				desktop.setCpu(cpu);
				desktop.setMotherboard(motherboard);
				
				PC pc = new PC(os, ram, harddrive, desktop);
				order.setPc(pc);
			}
			else
			{
				Laptop laptop = new Laptop();
				CPU cpu = new CPU();
				Motherboard motherboard = new Motherboard();
				Screen screen = new Screen();
				
				cpu.setSpeed("laptopCPU");
				motherboard.setType("laptopMotherboard");
				screen.setLaptopScreen(true);
				
				laptop.setCpu(cpu);
				laptop.setLaptopscreen(screen);
				laptop.setMotherboard(motherboard);
				
				PC pc = new PC(os, ram, harddrive, laptop);
				order.setPc(pc);
			}
			
			//Generate order quantity
			order.setQuantity((int) Math.floor(1+(50 * rand.nextFloat())));
			
			//Generate order price
			order.setPrice((int) (order.getQuantity() * Math.floor(600 + (200 * rand.nextFloat()))));
			
			//Generate order due date
			order.setDueindays((int) Math.floor(1 + (10 * rand.nextFloat())));
			
			DFAgentDescription manufacturerTemplate = new DFAgentDescription();
			ServiceDescription sd = new ServiceDescription();
			sd.setType("manufacturer");
			manufacturerTemplate.addServices(sd);
			try
			{
				DFAgentDescription[] agentsType1 = DFService.search(myAgent,  manufacturerTemplate);
				for(int i=0; i<agentsType1.length; i++)
				{
					manufacturerAgent = agentsType1[i].getName(); //this is the AID
					System.out.println("CUSTOMER: Manufacturer found: " + agentsType1[i].getName());
				}
				//manufacturerAgent = agentsType1[0].getName();
			}
			catch(FIPAException e)
			{
				e.printStackTrace();
			}
			
			ACLMessage msg = new ACLMessage(ACLMessage.REQUEST);
			msg.addReceiver(manufacturerAgent);
			msg.setLanguage(codec.getName());
			msg.setOntology(ontology.getName());
			try 
			{
				AgentAction s = new Send(myAgent.getAID(), order);
				Action actionOp = new Action(myAgent.getAID(), s);
				getContentManager().fillContent(msg, actionOp);
				//msg.setContentObject(order);
				myAgent.send(msg);
				System.out.println("CUSTOMER: Sent order to manufacturer");
				ordersSent++;
			} 
			catch (OntologyException e) 
			{
				e.printStackTrace();
			}
			catch (CodecException f)
			{
				f.printStackTrace();
			}
			
			boolean received = false;
			MessageTemplate mt = MessageTemplate.MatchContent("received");
			ACLMessage reply = myAgent.receive(mt);
			if(reply != null)
			{
				received = true;
				orderReceived++;
			}
		}
		
	}
	
	public class ReceiveOrder extends CyclicBehaviour
	{
		public ReceiveOrder(Agent a)
		{
			super(a);
		}
		
		@Override
		public void action()
		{
			//This behaviour should only respond to CONFIRM messages
			MessageTemplate mt = MessageTemplate.MatchPerformative(ACLMessage.CONFIRM);
			ACLMessage msg = receive(mt);
			if(msg != null)
			{
				try
				{
					System.out.println("CUSTOMER: Order received. Sending payment.");
					ContentElement ce = null;
					
					ce = getContentManager().extractContent(msg);
					if(ce instanceof Action)
					{
						Concept action = ((Action)ce).getAction();
						if (action instanceof Send)
						{
							//Reply with the cashola
							ACLMessage cash = new ACLMessage(ACLMessage.AGREE);
							for(AID manufacturer : manufacturers)
							{
								cash.addReceiver(manufacturer);	
							}
							cash.setContent("" + ((Send) action).getOrder().getPrice()); //oof
							myAgent.send(cash);
						}
					}
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		}
	}
	
	public class EndDay extends OneShotBehaviour 
	{
		private List<Behaviour> toRemove;
		
		public EndDay(Agent a, List<Behaviour> toRemove)
		{
			super(a);
			this.toRemove = toRemove;
		}
		
		@Override
		public void action()
		{
			ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
			msg.addReceiver(tickerAgent);
			msg.setContent("done");
			myAgent.send(msg);
			System.out.println("CUSTOMER: Day end, ticker agent informed");
			
			
			ACLMessage supplierDone = new ACLMessage(ACLMessage.INFORM);
			supplierDone.setContent("done");
			for(AID manufacturer : manufacturers)
			{
				supplierDone.addReceiver(manufacturer);
			}
			myAgent.send(supplierDone);
			System.out.println("CUSTOMER: Day end, manufacturer informed");
			
			//Remove the cyclic behaviours
			for (Behaviour b : toRemove)
			{
				myAgent.removeBehaviour(b);
			}
		}
	}
}

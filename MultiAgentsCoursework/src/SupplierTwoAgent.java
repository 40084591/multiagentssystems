import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import coursework_ontology.CourseworkOntology;
import coursework_ontology.elements.*;
import jade.content.ContentElement;
import jade.content.lang.Codec;
import jade.content.lang.sl.SLCodec;
import jade.content.onto.Ontology;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.SequentialBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

public class SupplierTwoAgent extends SupplierOneAgent
{
	
	private Codec codec = new SLCodec();
	private Ontology ontology = CourseworkOntology.getInstance();
	
	@Override
	protected void setup()
	{
		deliverindays = 3;
		getContentManager().registerLanguage(codec);
		getContentManager().registerOntology(ontology);
		//Add this agent to the yellow pages
		DFAgentDescription dfd = new DFAgentDescription();
		dfd.setName(getAID());
		ServiceDescription sd = new ServiceDescription();
		sd.setType("suppliertwo");
		sd.setName(getLocalName() + "-supplier-two");
		dfd.addServices(sd);
		try
		{
			DFService.register(this,  dfd);
		}
		catch(FIPAException e)
		{
			e.printStackTrace();
		}
		
		//TODO - Set prices
		addBehaviour(new TickerWaiter(this));
	}
	
	@Override
	public int calculatePrice(Order order)
	{
		int price = 0;
		//desktop or laptop?
		if (order.getPc().getDesktop() == null)
		{
			//is a laptop
			price = price + 370;
		}
		else if(order.getPc().getLaptop() == null)
		{
			//is a desktop
			price = price + 190;
		}
		
		//harddrive size
		if (order.getPc().getHarddrive().getCapacity() == 2)
		{
			price = price + 65;
		}
		else
		{
			price = price + 45;
		}
		
		//RAM size
		if (order.getPc().getRam().getCapacity() == 16)
		{
			price = price + 80;
		}
		else 
		{
			price = price + 40;
		}
		
		//OS type
		if (order.getPc().getOs().getVersion().equals("windows"))
		{
			price = price + 75;
		}

		return price * order.getQuantity();
	}
}

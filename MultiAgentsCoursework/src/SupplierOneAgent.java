import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import coursework_ontology.CourseworkOntology;
import coursework_ontology.elements.*;
import jade.content.AgentAction;
import jade.content.Concept;
import jade.content.ContentElement;
import jade.content.lang.Codec;
import jade.content.lang.sl.SLCodec;
import jade.content.onto.Ontology;
import jade.content.onto.basic.Action;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.SequentialBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import java.util.Iterator;

public class SupplierOneAgent extends Agent
{
	private AID tickerAgent;
	private ArrayList<AID> suppliers = new ArrayList<>();
	private ArrayList<Order> orderstofulfil = new ArrayList<>();
	private ArrayList<AID> manufacturers = new ArrayList<>();
	private boolean manufacturerFinished = false;
	
	private Codec codec = new SLCodec();
	private Ontology ontology = CourseworkOntology.getInstance();
	
	protected int deliverindays;
	
	@Override
	protected void setup()
	{
		deliverindays = 1;
		getContentManager().registerLanguage(codec);
		getContentManager().registerOntology(ontology);
		//Add this agent to the yellow pages
		DFAgentDescription dfd = new DFAgentDescription();
		dfd.setName(getAID());
		ServiceDescription sd = new ServiceDescription();
		sd.setType("supplierone");
		sd.setName(getLocalName() + "-supplier-one");
		dfd.addServices(sd);
		try
		{
			DFService.register(this,  dfd);
		}
		catch(FIPAException e)
		{
			e.printStackTrace();
		}
		
		//TODO - Set prices
		addBehaviour(new TickerWaiter(this));
	}
	
	protected void takeDown()
	{
		//Deregister from the yellow pages
		try 
		{
			DFService.deregister(this);
		}
		catch(FIPAException e)
		{
			e.printStackTrace();
		}
	}
	
	public class TickerWaiter extends CyclicBehaviour
	{
		//behaviour to wait for a new day
		public TickerWaiter(Agent a)
		{
			super(a);
		}
		
		@Override
		public void action()
		{
			MessageTemplate mt = MessageTemplate.or(MessageTemplate.MatchContent("new day"), MessageTemplate.MatchContent("terminate"));
			ACLMessage msg = myAgent.receive(mt);
			if(msg != null)
			{
				if(tickerAgent == null)
				{
					tickerAgent = msg.getSender();
				}
				if(msg.getContent().equals("new day"))
				{
					manufacturerFinished = false;
					//Each order needs to be ticked down, so we can tell which are to be sent out.
					for(Order order : orderstofulfil)
					{
						order.setDaystofulfil(order.getDaystofulfil()-1);
					}
					
					myAgent.addBehaviour(new FindManufacturer(myAgent));
					myAgent.addBehaviour(new FulfilOrder(myAgent));
					
					CyclicBehaviour sb = new SellBehaviour(myAgent);
					myAgent.addBehaviour(sb);
					ArrayList<Behaviour> cyclicBehaviour = new ArrayList<>();
					cyclicBehaviour.add(sb);
					myAgent.addBehaviour(new EndDayListener(myAgent, cyclicBehaviour));
				}
			}
		}
	}
	
	public class FindManufacturer extends OneShotBehaviour
	{
		public FindManufacturer(Agent a)
		{
			super(a);
		}
		
		@Override
		public void action()
		{
			DFAgentDescription manufacturerTemplate = new DFAgentDescription();
			ServiceDescription sd = new ServiceDescription();
			sd.setType("manufacturer");
			manufacturerTemplate.addServices(sd);
			try
			{
				manufacturers.clear();
				DFAgentDescription[] agentsType1 = DFService.search(myAgent, manufacturerTemplate);
				for(int i=0; i < agentsType1.length; i++)
				{
					manufacturers.add(agentsType1[i].getName());
				}
			}
			catch(FIPAException e)
			{
				e.printStackTrace();
			}
		}
	}
	
	public class SellBehaviour extends CyclicBehaviour
	{
		public SellBehaviour(Agent a)
		{
			super(a);
		}
		
		@Override
		public void action()
		{
			int price;
			AID manufacturerAgent;
			MessageTemplate mt = MessageTemplate.MatchPerformative(ACLMessage.REQUEST);
			ACLMessage msg =  myAgent.receive(mt);
			
			if(msg != null)
			{
				System.out.println("SUPPLIER: Order received.");
				try
				{

					manufacturerAgent = msg.getSender();
					ContentElement ce = null;
					
					ce = getContentManager().extractContent(msg);
					if (ce instanceof Action)
					{
						Concept action = ((Action)ce).getAction();
						if (action instanceof Send)
						{
							System.out.println("SUPPLIER: Calculating price...");
							orderstofulfil.add(((Send) action).getOrder());
							orderstofulfil.get(orderstofulfil.size()-1).setDaystofulfil(deliverindays);
							price = calculatePrice(((Send) action).getOrder());
							
							//return price to manufacturer
							ACLMessage totalprice = new ACLMessage(ACLMessage.PROPOSE);
							totalprice.addReceiver(manufacturerAgent);
							totalprice.setContent("" + price); //oof
							
							myAgent.send(totalprice);
							//After this, the orders will be fulfilled.
						}

					}
						
				} 
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		}
	}
	
	public class FulfilOrder extends OneShotBehaviour
	{
		public FulfilOrder(Agent a)
		{
			super(a);
		}
		
		@Override
		public void action()
		{
			//for(Order order : orderstofulfil)
			for(Iterator<Order> i = orderstofulfil.iterator(); i.hasNext();)
			{
				Order order = i.next();
				if (order.getDaystofulfil() <= 0)
				{
					//Send it
					ACLMessage fulfilment = new ACLMessage(ACLMessage.CONFIRM);
					for(AID manufacturer : manufacturers)
					{
						fulfilment.addReceiver(manufacturer);						
					}
					fulfilment.setLanguage(codec.getName());
					fulfilment.setOntology(ontology.getName());
					try
					{
						AgentAction s = new Send(myAgent.getAID(), order);
						Action actionOp = new Action(myAgent.getAID(), s);
						getContentManager().fillContent(fulfilment, actionOp);
						System.out.println("SUPPLIER: Order complete, sending");
						myAgent.send(fulfilment);
					}
					catch (Exception e)
					{
						e.printStackTrace();
					}
					i.remove();
				}
			}
		}
	}
	
	public class EndDayListener extends CyclicBehaviour
	{
		private List<Behaviour> toRemove;
		
		public EndDayListener(Agent a, List<Behaviour> toRemove)
		{
			super(a);
			this.toRemove = toRemove;
		}
		
		@Override
		public void action()
		{
			MessageTemplate mt = MessageTemplate.MatchContent("done");
			for(AID manufacturer : manufacturers)
			{
				mt.MatchSender(manufacturer);
			}

			ACLMessage msg = myAgent.receive(mt);
			
			if(msg != null)
			{
				manufacturerFinished = true;
			}
			else
			{
				block();
			}
			
			if (manufacturerFinished)
			{
				//Supplier is done for the day
				ACLMessage tick = new ACLMessage(ACLMessage.INFORM);
				tick.setContent("done");
				tick.addReceiver(tickerAgent);
				myAgent.send(tick);
				System.out.println("SUPPLIER: Day end, ticker agent informed");
				
				//Remove the cyclic behaviours
				for (Behaviour b : toRemove)
				{
					myAgent.removeBehaviour(b);
				}
				manufacturerFinished = false;
				//Something is broken so this will clear the queue
				while (myAgent.receive() != null)
				{
					System.out.println("SUPPLIER: Clearing queue");
				}
				myAgent.removeBehaviour(this);
			}
		}
	}
	
	public int calculatePrice(Order order)
	{
		int price = 0;

		//desktop or laptop?
		if (order.getPc().getDesktop() == null)
		{
			//is a laptop
			price = price + 425;
		}
		else if(order.getPc().getLaptop() == null)
		{
			//is a desktop
			price = price + 225;
		}
		
		//harddrive size
		if (order.getPc().getHarddrive().getCapacity() == 2)
		{
			price = price + 75;
		}
		else
		{
			price = price + 50;
		}
		
		//RAM size
		if (order.getPc().getRam().getCapacity() == 16)
		{
			price = price + 90;
		}
		else 
		{
			price = price + 50;
		}
		
		//OS type
		if (order.getPc().getOs().getVersion().equals("windows"))
		{
			price = price + 75;
		}
		return price * order.getQuantity();
	}
}
